# Push Sender Java

Утилита на java, которая предоставляет возможность взаимодействия с Push-сервером.

## Зависимости

+ Gradle версии >= 6
+ OpenJDK совместимая с используемой версией Gradle

## Использование

1. Для сборки необходимо выполнить команду в корневом каталоге текущего проекта:
```bash
gradle build jar
```

2. Запуск
- Утилита использует файл с конфигурацией, полученный от владельца Push-сервера, который имеет вид [config.yml](src/main/resources/config.yml).
- Команда для запуска тестового приложения выглядит так:
```bash
java -jar ./build/libs/sample-push-client-java-1.0.0.jar 
      --action one of:
            * send - отправить сообщение (используется по умолчанию)
            * get-project - получить сведения о проекте
            * update-key - сгенерировать новую пару ключей, обновить пуличный ключ для service account и отправить тестовое сообщение (в случае если параметр --regID задан) 
      --config <путь к app_server_xxx.yml, выгруженному из настроек Push-проекта (по умолчанию config.yml)>
      --regID <registration ID, полученный в push-демоне или эмуляторе при регистрации (по умолчанию e3bbd700-6a76-4ad6-a699-ac284da5b1ab для проверки TARGET NOT FOUND)>
      --messageText <сообщение для тела push-уведомления ("some text" по умолчанию)>
```

Например, **для получения токена и отправки тестового push-сообщения**, находясь в корневом каталоге текущего проекта, нужно выполнить команду вида:
```bash
java -jar ./build/libs/sample-push-client-java-1.0.0.jar \
      --config app_server_kaa_test_ckr3su9pfb78c202gffg.yml --action send --regID cbba8bc0-4756-446c-8cce-5140bee8e49d
```
где **cbba8bc0-4756-446c-8cce-5140bee8e49d** -- registrationId, полученный при подключении приложения (на мобильном устройстве или эмуляторе) к push-серверу.
 
После успешной отправки программа выведет примерно такой текст:
```bash
 ⇒   java -jar ./build/libs/sample-push-client-java-1.0.0.jar ./config.test.yml cbba8bc0-4756-446c-8cce-5140bee8e49d
ru.omprussia.omp.push.ru.omp.push.example.config.Config{server=Server{projectId='test_push_proje_bupp7gvl5abbcb1fj5tg', clientId='test_push_proje_bupp7gvl5abbcb1fj5tg', privateKeyId='public:oBSzhKTtZZ', privateKey='-----BEGIN RSA PRIVATE KEY-----
MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDyfjiSrh56AjWh
................................................................
JAwoGEnA0rAC2YlujStrsR3fO6SDCWnJxqFuW6zbcvhztT+HhKjJDd5Hl0jBV/w2
36U/xP7DI8hYeyY/dSwWwyPq2auuPso2
-----END RSA PRIVATE KEY-----
', scope='openid offline message:update', audience='ocs-auth-public-api-gw ocs-push-public-api-gw', pushAddress='http://ocs-push-dev.devel.pro:8009/push/public', authAddress='http://ocs-push-dev.devel.pro:8009/push/public'}, client=Client{applicationId='test_client_bupp7kvl5abbcb1fj5u0'}}
ACCESS TOKEN: eyJhbGciOiJSUzI1NiIsImtpZCI6InB1YmxpYzplZDhhYmIzNy0yZjVmLTRiNTktYTNmZi1jMTViODBkMzEzNGEiLCJ0eXAiOiJKV1QifQ.eyJhdWQiOlsib2NzLWF1dGgtcHVibGljLWFwaS1ndyIsIm9jcy1wdXNoLXB1YmxpYy1hcGktZ3ciXSwiY2xpZW50X2lkIjoidGVzdF9wdXNoX3Byb2plX2J1cHA3Z3ZsNWFiYmNiMWZqNXRnIiwiZXhwIjoxNjA1ODg3MjMwLCJleHQiOnt9LCJpYXQiOjE2MDU4ODM2MzAsImlzcyI6Imh0dHA6Ly9vY3MtcHVzaC1kZXYub21wY2xvdWQucnUvYXV0aC9wdWJsaWMvIiwianRpIjoiMzdkOGQ0NGQtY2I5ZS00NGRiLWI4ZDgtMDA5Mzc3YjE1MDg0IiwibmJmIjoxNjA1ODgzNjMwLCJzY3AiOlsib3BlbmlkIiwib2ZmbGluZSIsIm1lc3NhZ2U6dXBkYXRlIl0sInN1YiI6InRlc3RfcHVzaF9wcm9qZV9idXBwN2d2bDVhYmJjYjFmajV0ZyJ9.lvWjNKv0AjKvDp-PfiG29W_NanjzXW3mcT71wMnVum9eROai-A9ptnSP9hZxj3cWMSNzgyCVHsXrlFmH221-vlRpVmqNIumGww3B5Gxi47UcjXKnaDIt5LSHC78EQfk2GVa5FhsOVG9Slag_ppTVofA3V8JNQ-YaTePo2u2AGS-ThPVdbSV72fa1AnvkWBudsjxBk5EvtDGhs5ZyzqBZHUfdMXci1aS2LrMo_3IItk36IuTtTgGKgsZ5H5P-_49-DbSMjAWGEkXeU37Wnl9gk-IXemKta5JlLjKiT2jCbUvH-3XdE4IjjecaGMBwTGpvjwa4CFyJcK8g1ptt-QdEX299VZTv9Di_Dj-kP1_QM0ggs1vB83_YMONIwGjtyyK9IsHp0c4XuGW-_G-WlheNLAdTeGjompxSAz_NKOHkGv1DdAzII8IJ49sH0IiUY1oUonDMkOPSlkO2qdhwSX-h04p20GjQ_ASAYTLnw3nvJUPKs4nISDkp7DsevSaKXI0Hy0PvCT3zEK4ozCipGUi2achpSQgjlk8CHyjtsyTzepK7tta6U2ZLfN_DY5zirD6b122Ow-bQvMxI0Xr6af4f6-p4ksVh_WWVu8M8vXiBXslZTEI_8859ekV7eyWGLBTqrpYcamtVQ8fkat_hPvGYF2BN-2s-nxGoyefgIxYvRig


Validating token...
Token VALID

Sending push-message:
HTTP Code: 202
Response: {
  "createdAt": "0001-01-01T00:00:00Z",
  "expiredAt": "2020-11-20T17:59:24.803836Z",
  "id": "56b7522f-cded-4f0a-8d63-bf17809e4b5f",
  "notification": {
    "createdAt": "0001-01-01T00:00:00Z",
    "data": {
      "action": "command",
      "another_key": "value"
    },
    "message": "some message",
    "title": "some title",
    "updatedAt": "0001-01-01T00:00:00Z"
  },
  "target": "cbba8bc0-4756-446c-8cce-5140bee8e49d",
  "type": "device",
  "updatedAt": "0001-01-01T00:00:00Z"
}

SUCCESS
```

**Для получения информации о проекте**, находясь в корневом каталоге текущего проекта, нужно выполнить команду вида:
```bash
java -jar ./build/libs/sample-push-client-java-1.0.0.jar \
      --config app_server_kaa_test_ckr3su9pfb78c202gffg.yml --action get-project
```

**Для смены ключа** команда может выглядеть так:
```bash
java -jar ./build/libs/sample-push-client-java-1.0.0.jar \
      --config app_server_kaa_test_ckr3su9pfb78c202gffg.yml --action update-key --regID cbba8bc0-4756-446c-8cce-5140bee8e49d
```
Если в команде выше параметр regID не указан, то отправки тестового сообщения после смены ключа не будет.

## Токен авторизации

Перед тем, как делать запрос на отправку push-сообщений, нужно проверить токен на актуальность. Для этого можно воспользоваться методом [validateToken](src/main/java/ru/omp/push/example/auth/TokenFetcher.java#L46).

В случае если запрос на отправку push-сообщения вернул `HTTP code 401`, то значит токен невалиданый и его нужно обновить с помощью метода [authenticate](src/main/java/ru/omp/push/example/auth/TokenFetcher.java#L57) и произвести попытку отправить push-сообщение. Данное поведение может зависеть от стратегии сервера приложений.

## Условия использования и участия

Исходный код проекта предоставляется по [лицензии](LICENSE.BSD-3-Clause.md),
которая позволяет использовать его в сторонних приложениях.

[Соглашение участника](CONTRIBUTING.md) регламентирует права,
предоставляемые участниками компании «Открытая Мобильная Платформа».

Информация об участниках указана в файле [AUTHORS](AUTHORS.md).

[Кодекс поведения](CODE_OF_CONDUCT.md) — это действующий набор правил
компании «Открытая Мобильная Платформа»,
который информирует об ожиданиях по взаимодействию между членами сообщества при общении и работе над проектами.

## Структура проекта
* Каталог **[example](src/main/java/ru/omp/push/example)** содержит исходный код утилиты.
  * Файл **[TestPushClient.java](src/main/java/ru/omp/push/example/TestPushClient.java)** определяет класс, реализующий функциональность push-клиента.
  * Файл **[CLIParser.java](src/main/java/ru/omp/push/example/CLIParser.java)** определяет класс, реализующий разбор команд, полученных из CLI.
  * Файл **[Config.java](src/main/java/ru/omp/push/example/config/Config.java)** определяет класс для загрузки конфигурации push-проекта.
  * Файл **[TokenFetcher.java](src/main/java/ru/omp/push/example/auth/TokenFetcher.java)** определяет класс для взаимодействия с токеном авторизации.
* Файл **[config.yml](src/main/resources/config.yml)** определяет пример конфигурации файла push-проекта.

## This document in English

- [README.md](README.md)

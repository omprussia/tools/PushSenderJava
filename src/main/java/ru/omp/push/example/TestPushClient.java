// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example;

import kong.unirest.ContentType;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import ru.omp.push.example.auth.TokenFetcher;
import ru.omp.push.example.config.Config;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class TestPushClient {

    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private final Config cfg;
    private final String configFile;
    private final TokenFetcher tokenFetcher;

    private String getToken() throws Exception {
        return this.tokenFetcher.authenticate();
    }

    private int sendMessage(String accessToken, String registrationId, String message) {
        String url = cfg.getServer().getApiUrl()
                + "/projects/" + cfg.getServer().getProjectId() + "/messages";

        if (message == null || message.length() == 0) {
            message = "some message";
        }

        String json = "{\n" +
                "  \"target\": " + registrationId + ",\n" +
                "  \"ttl\": \"2h\",\n" +
                "  \"type\": \"device\",\n" +
                "  \"notification\": {\n" +
                "    \"title\": \"some title\",\n" +
                "    \"message\": \"" + message + "\",\n" +
                "    \"data\": {\n" +
                "      \"action\": \"command\",\n" +
                "      \"another_key\": \"value\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        HttpResponse<JsonNode> jsonResponse =
                Unirest.post(url)
                        .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                        .header("Authorization", "Bearer " + accessToken)
                        .body(new JsonNode(json)).asJson();

        int code = jsonResponse.getStatus();
        String response = jsonResponse.getBody().toPrettyString();
        System.out.println("Send message response:");
        System.out.println("\tHTTP Code: " + code);
        System.out.println("\tBody: " + response + "\n");
        return code;
    }

    private int getProjectDetails(String accessToken) {
        String url = cfg.getServer().getApiUrl()
                + "/projects/" + cfg.getServer().getProjectId();

        HttpResponse<JsonNode> jsonResponse =
                Unirest.get(url)
                        .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                        .header("Authorization", "Bearer " + accessToken)
                        .asJson();

        int code = jsonResponse.getStatus();
        String response = jsonResponse.getBody().toPrettyString();
        System.out.println("Project details response:");
        System.out.println("\tHTTP Code: " + code);
        System.out.println("\tBody: " + response + "\n");
        return code;
    }

    private String genRandomStr(int length) {
        char[] arr = new char[length];
        int alphaLen = ALPHA.length();
        for (int i=0; i<length; i++) {
            arr[i] = ALPHA.charAt(new Random().nextInt(alphaLen));
        }
        return new String(arr);
    }

    private KeyPairResponse generateKeyPair(String accessToken) {
        String url = cfg.getServer().getApiUrl() + "/keyPairs";
        String kid = genRandomStr(10);
        String body = "{\"alg\": \"RS256\", \"kid\": \"" + kid + "\", \"use\": \"sig\"}";

        HttpResponse<JsonNode> jsonResponse =
                Unirest.post(url)
                        .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                        .header("Authorization", "Bearer " + accessToken)
                        .body(body)
                        .asJson();

        int code = jsonResponse.getStatus();
        JsonNode json = jsonResponse.getBody();
        String response = json.toPrettyString();
        System.out.println("Key Pair response:");
        System.out.println("\tHTTP Code: " + code);
        System.out.println("\tBody: " + response + "\n");

        if (code == 200) {
            String pem = (String) json.getObject().getJSONObject("private").get("pem");
            String newKid = (String) json.getObject().getJSONObject("public").getJSONObject("jwk").get("kid");
            String jwk = json.getObject().getJSONObject("public").getJSONObject("jwk").toString();
            return new KeyPairResponse(code, newKid, pem, jwk);
        }

        return new KeyPairResponse(code);
    }

    private int applyPublicKey(String accessToken, String kid, String pem, String jwk) throws IOException {
        String url = cfg.getServer().getApiUrl()
                + "/projects/" + cfg.getServer().getProjectId()
                + "/serviceAccounts/" + cfg.getServer().getClientId()
                + "/publicKeys";

        String body = "{\"keys\": [" + jwk + "]}";
        HttpResponse<JsonNode> jsonResponse =
                Unirest.put(url)
                        .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                        .header("Authorization", "Bearer " + accessToken)
                        .body(body)
                        .asJson();

        int code = jsonResponse.getStatus();
        JsonNode json = jsonResponse.getBody();
        String response = json.toPrettyString();
        System.out.println("Update Public Key response:");
        System.out.println("\tHTTP Code: " + code);
        System.out.println("\tBody: " + response + "\n");

        if (code == 200) {
            System.out.println("Updating config file...");
            System.out.println("\tBackup " + this.configFile + " to " + this.configFile + "_backup");
            new File(this.configFile).renameTo(new File(this.configFile + "_backup"));
            this.cfg.getServer().setPrivateKey(pem);
            this.cfg.getServer().setKeyId(kid);
            this.cfg.save(this.configFile);

            // update keys in token fetcher
            this.tokenFetcher.updateKeys(kid, pem);
        }

        return code;
    }

    public TestPushClient(String configPath) throws IOException {
        this.configFile = configPath;
        Config cfg = Config.load(configPath);
        System.out.println(cfg);
        this.cfg = cfg;
        this.tokenFetcher = new TokenFetcher(cfg);

        // for test purposes in case of self-signed server certificates
        Unirest.config().verifySsl(false);
    }

    public static void main(String[] args) {
        CLIParser cliArgs = CLIParser.parseArguments(args);

        try {
            TestPushClient client = new TestPushClient(cliArgs.configFile);

            switch (cliArgs.action) {
                case SEND: {
                    String accessToken = client.getToken();
                    System.out.println("ACCESS TOKEN: " + accessToken);

                    // Validate token for expiration.
                    // This code block is offline validation of token before real request to send push
                    System.out.println("\nValidating token...");
                    if (client.tokenFetcher.validateToken(accessToken)) {
                        System.out.println("Token VALID");
                    } else {
                        System.out.println("TOKEN not valid. Please ask for new");
                        accessToken = client.getToken();
                    }

                    System.out.println("\nSending push-message:");
                    int code = client.sendMessage(accessToken, cliArgs.registrationId, cliArgs.messageText);
                    if (code == 401) {
                        System.out.println("Need to refresh token and make request ");
                        /*
                          In real life in case of 401 we have to get new access token and make request again,
                          but it depends on application server strategy, i.e.:
                             accessToken = client.getToken();
                             code = client.sendMessage(accessToken, registrationId);
                         */
                    } else if (code < 200 || code > 299) {
                        System.out.println("FAILED");
                    } else {
                        System.out.println("SUCCESS");
                    }

                    break;
                }
                case GET_PROJECT: {
                    String accessToken = client.getToken();
                    System.out.println("ACCESS TOKEN: " + accessToken);

                    int code = client.getProjectDetails(accessToken);
                    if (code == 401) {
                        System.out.println("Need to refresh token and make request ");
                        /*
                          In real life in case of 401 we have to get new access token and make request again,
                          but it depends on application server strategy, i.e.:
                             accessToken = client.getToken();
                             code = client.sendMessage(accessToken, registrationId);
                         */
                    } else if (code < 200 || code > 299) {
                        System.out.println("FAILED");
                    } else {
                        System.out.println("SUCCESS");
                    }

                    break;
                }
                case UPDATE_KEY: {
                    String accessToken = client.getToken();
                    System.out.println("ACCESS TOKEN: " + accessToken);

                    KeyPairResponse keyPairResponse = client.generateKeyPair(accessToken);
                    if (keyPairResponse.code == 200) {
                        int code =
                                client.applyPublicKey(
                                        accessToken, keyPairResponse.publicKey,
                                        keyPairResponse.privateKey,
                                        keyPairResponse.jwk
                                );
                        if (code == 200) {
                            if (cliArgs.registrationId != null) {
                                // send push with OLD token
                                client.sendMessage(accessToken, cliArgs.registrationId, cliArgs.messageText);

                                String newAccessToken = client.getToken();
                                // send push with NEW token
                                client.sendMessage(newAccessToken, cliArgs.registrationId, cliArgs.messageText);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}

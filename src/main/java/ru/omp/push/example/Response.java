// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example;

public class Response {

    int code;

    Response(int code) {
        this.code = code;
    }
}

// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Getter
@Setter
@NoArgsConstructor
public class Config {

    @JsonProperty("push_notification_system")
    private PushNotificationSystem server;

    public static Config load(String configPath) throws IOException {
        InputStream inputStream = new FileInputStream(configPath);
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        Config serverConfig = om.readValue(inputStream, Config.class);
        return serverConfig;
    }

    public void save(String configPath) throws IOException {
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.writeValue(new File(configPath), this);
    }
}

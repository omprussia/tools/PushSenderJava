// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PushNotificationSystem {

    @JsonProperty("project_id")
    private String projectId;

    @JsonProperty("push_public_address")
    private String pushPublicAddress;

    @JsonProperty("api_url")
    private String apiUrl;

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("scopes")
    private String scopes;

    @JsonProperty("audience")
    private String audience;

    @JsonProperty("token_url")
    private String tokenUrl;

    @JsonProperty("private_key")
    private String privateKey;

    @JsonProperty("key_id")
    private String keyId;
}

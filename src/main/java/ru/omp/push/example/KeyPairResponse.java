// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example;

public class KeyPairResponse extends Response {

    String publicKey;
    String privateKey;
    String jwk;

    KeyPairResponse(int code, String publicKey, String privateKey, String jwk) {
        super(code);
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.jwk = jwk;
    }

    KeyPairResponse(int code) {
        super(code);
    }
}

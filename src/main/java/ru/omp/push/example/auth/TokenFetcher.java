// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example.auth;

import kong.unirest.*;
import kong.unirest.json.JSONObject;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import ru.omp.push.example.config.Config;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;

public class TokenFetcher {

    private final String apiUrl;
    private final String clientId;
    private final String scope;
    private String privateKey;
    private String privateKeyId;
    private final String audience;

    public TokenFetcher(Config cfg) {
        this.audience = cfg.getServer().getAudience();
        this.clientId = cfg.getServer().getClientId();
        this.privateKey = cfg.getServer().getPrivateKey();
        this.privateKeyId = cfg.getServer().getKeyId();
        this.apiUrl = cfg.getServer().getApiUrl();
        this.scope = cfg.getServer().getScopes();
    }

    public void updateKeys(String kid, String privateKey) {
        this.privateKey = privateKey;
        this.privateKeyId = kid;
    }

    public boolean validateToken(String accessToken) throws Exception {
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setSkipSignatureVerification()
                .setExpectedAudience(this.audience.split(" "))
                .build();

        JwtClaims jwtClaims = jwtConsumer.processToClaims(accessToken);
        // 60 seconds - maximum timeout
        return !jwtClaims.getExpirationTime().isBefore(NumericDate.fromMilliseconds((new Date()).getTime() + 60000));
    }

    public String authenticate() throws Exception {
        HttpResponse<JsonNode> wellKnownResponse = Unirest.get(apiUrl + "/.well-known").asJson();
        String wellKnownResponseJs = wellKnownResponse.getBody().getObject().get("token_endpoint").toString();

        // Load the private key to sign the request JWT
        PrivateKey signingKey = takeSigningKey();

        // generate the privateKeyJWT to authenticate at the token endpoint
        String privateKeyJWT = generatePrivateKeyJWT(signingKey, wellKnownResponseJs);

        // get the access token
        return getAccessToken(privateKeyJWT, wellKnownResponseJs);
    }

    protected String getAccessToken(String privateKeyJWT, String wellKnownResponseJs) throws Exception {
        // send the client_credentials request
        HttpResponse<JsonNode> jsonResponse = Unirest.post(wellKnownResponseJs)
                .field("grant_type", "client_credentials")
                .field("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer").field("client_assertion", privateKeyJWT)
                .field("scope", scope)
                .field("audience", audience)
                .asJson();

        // stop if response code is not HTTP 200
        if (jsonResponse.getStatus() != 200) {
            throw new Exception("Authentication failed");
        }

        JSONObject accessToken = jsonResponse.getBody().getObject();
        return accessToken.get("access_token").toString();
    }

    protected String generatePrivateKeyJWT(PrivateKey rsaPrivate, String audience) throws Exception {
        // generate the claims for the JWT body
        JwtClaims claims = new JwtClaims();
        claims.setIssuer(clientId);
        claims.setSubject(clientId);
        claims.setAudience(audience);
        claims.setExpirationTimeMinutesInTheFuture(5);
        claims.setIssuedAtToNow();
        claims.setGeneratedJwtId();

        // get the signed JWT
        return getJwt(privateKeyId, rsaPrivate, claims);
    }

    protected String getJwt(String keyId, PrivateKey rsaPrivate, JwtClaims claims) throws JoseException {
        JsonWebSignature jws = new JsonWebSignature();

        // set the claims
        jws.setPayload(claims.toJson());

        // set the signing key
        jws.setKey(rsaPrivate);
        // set header
        jws.setKeyIdHeaderValue(keyId);
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        // generate and return JWT
        return jws.getCompactSerialization();
    }

    protected PrivateKey takeSigningKey() throws Exception {
        String clearPrivateKey = privateKey
                .replace("-----BEGIN RSA PRIVATE KEY-----", "")
                .replaceAll("\n", "")
                .replace("-----END RSA PRIVATE KEY-----", "");


        byte[] decoded = Base64.getDecoder().decode(clearPrivateKey);
        return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
    }
}

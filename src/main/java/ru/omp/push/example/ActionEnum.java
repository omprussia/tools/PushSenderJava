// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example;

public enum ActionEnum {

    SEND,
    UPDATE_KEY,
    GET_PROJECT
    
}

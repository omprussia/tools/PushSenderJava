// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

package ru.omp.push.example;

import org.apache.commons.cli.*;

import java.io.File;

public class CLIParser {

    ActionEnum action;
    String configFile;
    String registrationId;
    String messageText;

    private CLIParser(ActionEnum action, String configFile, String registrationId, String messageText) {
        this.action = action;
        this.configFile = configFile;
        this.registrationId = registrationId;
        this.messageText = messageText;
    }

    static CLIParser parseArguments(String [] args) {
        Options options = new Options();
        Option config = Option.builder("c").longOpt("config")
                .argName("config")
                .hasArg()
                .required(true)
                .desc("path to YAML file with PUSH project configuration").build();
        options.addOption(config);

        Option action = Option.builder("a").longOpt("action")
                .argName("action")
                .hasArg()
                .required(false)
                .desc("one of \"send\" (send message - used by default), \"get-project\" (get project details), '\n" +
                        "        '\"update-key\" (generate new pair and update public key)").build();
        options.addOption(action);

        Option regID = Option.builder("r").longOpt("regID")
                .argName("regID")
                .hasArg()
                .required(false)
                .desc("registration ID to send push message").build();

        options.addOption(regID);

        Option messageText = Option.builder("m").longOpt("messageText")
                .argName("messageText")
                .hasArg()
                .required(false)
                .desc("Text for push message").build();
        options.addOption(messageText);

        // define parser
        CommandLine cmd;
        CommandLineParser parser = new DefaultParser();

        try {
            cmd = parser.parse(options, args);
            ActionEnum actionValue = ActionEnum.SEND;
            if(cmd.hasOption("a")) {
                switch (cmd.getOptionValue("a").toLowerCase()) {
                    case "update-key":
                        actionValue = ActionEnum.UPDATE_KEY;
                        break;
                    case "get-project":
                        actionValue = ActionEnum.GET_PROJECT;
                        break;
                }
            }

            String configFile = cmd.getOptionValue("c");
            if (!new File(configFile).exists()) {
                throw new IllegalArgumentException("Config file `" + configFile + "` not found");
            }

            String messageTextValue = cmd.hasOption("m") ? cmd.getOptionValue("m") : "some text";
            String regIDValue = cmd.hasOption("r") ? cmd.getOptionValue("r") : null;
            if (actionValue == ActionEnum.SEND && regIDValue == null) {
                throw new IllegalArgumentException("Registration ID is required when trying to send message");
            }

            return new CLIParser(
                actionValue, configFile, regIDValue, messageTextValue
            );
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            new HelpFormatter().printHelp("Usage:", options);
            System.exit(1);
        }

        return null;
    }
}
